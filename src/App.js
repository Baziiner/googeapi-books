import React from "react";
import { HelmetProvider } from "react-helmet-async";
import { Switch, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import ROUTES from "./view/routes";
import SideBar from "./components/Sidebar";
import Details from "./view/Details";
import Favourites from "./view/Favourites";
import Settings from "./view/Settings";
import Marvel from "./view/Marvel";
import Home from "./view/Home";

function App() {
  const themeDark = useSelector((state) => state.theme.isDark);
  return (
    <div className={`App ${themeDark ? "dark-theme" : "light-theme"}`}>
      <SideBar />
      <HelmetProvider>
        <div className="main-content">
          <Switch>
            <Route exact path={ROUTES.HOME} component={Home} />
            <Route path={ROUTES.DETAILS} component={Details} />
            <Route path={ROUTES.FAVOURITES} component={Favourites} />
            <Route path={ROUTES.SETTINGS} component={Settings} />
            <Route path={ROUTES.MARVEL} component={Marvel} />
          </Switch>
        </div>
      </HelmetProvider>
    </div>
  );
}

export default App;
