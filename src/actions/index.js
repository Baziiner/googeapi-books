import axios from "axios";
import actionTypes from "./actionTypes";

export const getBooks = (value) => {
  let fetchValue = value.split(" ").join("+");
  return (dispatch) => {
    dispatch({ type: actionTypes.GETBOOKS_START });
    axios
      .get(
        `https://www.googleapis.com/books/v1/volumes?q=${fetchValue}&key=AIzaSyAIKKVB6KnMOAsrRyEAyGWeoGD_BcvlDGM`
      )
      .then((res) => {
        if (res.data.totalItems !== 0) {
          dispatch({
            type: actionTypes.GETBOOKS_SUCCESS,
            payload: res.data.items,
            title: value,
          });
        } else {
          dispatch({
            type: actionTypes.GETBOOKS_ERROR,
            payload: {
              error: true,
              errorMsg: "No Book Found",
            },
          });
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const getFeaturedBookList = (value) => {
  let fetchValue = value.split(" ").join("+");
  return (dispatch) => {
    dispatch({ type: actionTypes.GET_FEATURED_START });
    axios
      .get(
        `https://www.googleapis.com/books/v1/volumes?q=${fetchValue}&maxResults=4&key=AIzaSyAIKKVB6KnMOAsrRyEAyGWeoGD_BcvlDGM`
      )
      .then((res) => {
        dispatch({
          type: actionTypes.GET_FEATURED_SUCCESS,
          payload: res.data.items,
          title: value,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const clearFeaturedBookList = () => {
  return {
    type: actionTypes.CLEAR_FEATURED,
  };
};

export const getBookByID = (id) => {
  return (dispatch) => {
    dispatch({ type: actionTypes.GETBOOK_ID_START });
    axios
      .get(
        `https://www.googleapis.com/books/v1/volumes/${id}?key=AIzaSyAIKKVB6KnMOAsrRyEAyGWeoGD_BcvlDGM`
      )
      .then((res) => {
        dispatch({
          type: actionTypes.GETBOOK_ID_SUCCESS,
          payload: res.data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const addFavourite = (data) => {
  return {
    type: actionTypes.ADD_TO_FAVOURITES,
    payload: data,
  };
};

export const removeFavourite = (id) => {
  return {
    type: actionTypes.REMOVE_FROM_FAVOURITES,
    payload: id,
  };
};

export const searchFromInput = (value) => {
  let fetchValue = value.split(" ").join("+");
  return (dispatch) => {
    dispatch({ type: actionTypes.GETBOOKS_START });
    axios
      .get(
        `https://www.googleapis.com/books/v1/volumes?q=${fetchValue}&key=AIzaSyAIKKVB6KnMOAsrRyEAyGWeoGD_BcvlDGM`
      )
      .then((res) => {
        if (res.data.totalItems !== 0) {
          dispatch({
            type: actionTypes.GETBOOKS_SUCCESS,
            payload: res.data.items,
          });
          dispatch({
            type: actionTypes.ADD_TO_RANDOM_LIST,
            payload: value,
          });
        } else {
          dispatch({
            type: actionTypes.GETBOOKS_ERROR,
            payload: {
              error: true,
              errorMsg: "No Book Found",
            },
          });
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const setTheme = (data) => {
  return {
    type: actionTypes.SET_THEME,
    payload: data,
  };
};

export const getMarvelHero = (value) => {
  let fetchValue = value.split(" ").join("+");
  return (dispatch) => {
    dispatch({ type: actionTypes.GET_MARVELHERO_START });
    axios
      .get(
        `https://gateway.marvel.com/v1/public/characters?name=${fetchValue}&apikey=0ac4d72cd75077312853bebbdaae1bfe`
      )
      .then((res) => {
        dispatch({
          type: actionTypes.GET_MARVELHERO_SUCCESS,
          payload: res.data.data.results[0],
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const clearMarvelHero = () => {
  return {
    type: actionTypes.CLEAR_MARVELHERO,
  };
};

export const getMarvelDetails = (id) => {
  let urlComic = `https://gateway.marvel.com/v1/public/characters/${id}/comics?limit=10&apikey=0ac4d72cd75077312853bebbdaae1bfe`;
  let urlEvent = `https://gateway.marvel.com/v1/public/characters/${id}/events?limit=10s&apikey=0ac4d72cd75077312853bebbdaae1bfe`;
  let urlSeries = `https://gateway.marvel.com/v1/public/characters/${id}/series?limit=10&apikey=0ac4d72cd75077312853bebbdaae1bfe`;

  const requestOne = axios.get(urlComic);
  const requestTwo = axios.get(urlEvent);
  const requestThree = axios.get(urlSeries);

  return (dispatch) => {
    dispatch({ type: actionTypes.GET_MARVELDETAILS_START });
    axios
      .all([requestOne, requestTwo, requestThree])
      .then(
        axios.spread((...responses) => {
          const responseOne = responses[0];
          const responseTwo = responses[1];
          const responseThree = responses[2];
          dispatch({
            type: actionTypes.GET_MARVELDETAILS_COMICS_SUCCESS,
            payload: responseOne.data.data.results,
            id: id,
          });
          dispatch({
            type: actionTypes.GET_MARVELDETAILS_EVENTS_SUCCESS,
            payload: responseTwo.data.data.results,
            id: id,
          });
          dispatch({
            type: actionTypes.GET_MARVELDETAILS_SERIES_SUCCESS,
            payload: responseThree.data.data.results,
            id: id,
          });
          dispatch({
            type: actionTypes.GET_MARVELDETAILS_SUCCESS,
          });
        })
      )
      .catch((errors) => {
        console.log(errors);
      });
  };
};
