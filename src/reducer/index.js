import { combineReducers } from "redux";
import booksReducer from "./books";
import favouritesReducer from "./favourites";
import randomReducer from "./random";
import themeReducer from "./theme";
import marvelReducer from "./marvel";

const rootReducer = combineReducers({
  book: booksReducer,
  favourites: favouritesReducer,
  randomPool: randomReducer,
  theme: themeReducer,
  marvel: marvelReducer,
});

export default rootReducer;
