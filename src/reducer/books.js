import actionTypes from "../actions/actionTypes";

const initialState = {
  data: {
    isFetching: true,
    error: {
      errMsg: "",
      isError: false,
    },
    items: [],
    title: "",
  },
  bookID: {
    isFetching: true,
    item: {},
  },
  featured: {
    isFetching: true,
    items: [],
  },
};

const booksReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GETBOOKS_START:
      return {
        ...state,
        data: {
          ...state.data,
          isFetching: true,
          error: {
            errorMsg: "",
            isError: false,
          },
        },
      };
    case actionTypes.GETBOOKS_SUCCESS:
      return {
        ...state,
        data: {
          isFetching: false,
          items: action.payload,
          title: action.title,
          error: {
            errorMsg: "",
            isError: false,
          },
        },
      };
    case actionTypes.GETBOOKS_ERROR:
      return {
        ...state,
        data: {
          isFetching: false,
          title: action.title,
          error: {
            isError: action.payload.error,
            errorMsg: action.payload.errorMsg,
          },
          items: [],
        },
      };
    case actionTypes.GETBOOK_ID_START:
      return {
        ...state,
        bookID: {
          ...state.bookID,
          isFetching: true,
        },
      };
    case actionTypes.GETBOOK_ID_SUCCESS:
      return {
        ...state,
        bookID: {
          isFetching: false,
          item: action.payload,
        },
      };
    case actionTypes.GET_FEATURED_START:
      return {
        ...state,
        featured: {
          items: [],
          isFetching: true,
        },
      };
    case actionTypes.GET_FEATURED_SUCCESS:
      return {
        ...state,
        featured: {
          isFetching: false,
          items: [
            ...state.featured.items,
            [
              {
                title: action.title,
                data: action.payload,
              },
            ],
          ],
        },
      };
    case actionTypes.CLEAR_FEATURED:
      return {
        ...state,
        featured: {
          isFetching: true,
          items: [],
        },
      };
    default:
      return state;
  }
};

export default booksReducer;
