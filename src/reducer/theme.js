import actionTypes from "../actions/actionTypes";

const initialState = {
  isDark: false,
};

const themeReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_THEME:
      return {
        ...state,
        isDark: action.payload,
      };
    default:
      return state;
  }
};

export default themeReducer;
