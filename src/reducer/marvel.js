import actionTypes from "../actions/actionTypes";

const initialState = {
  heroes: {
    isFetching: true,
    data: [],
  },
};

const marvelReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_MARVELHERO_START:
      return {
        ...state,
        heroes: {
          ...state.heroes,
          isFetching: true,
        },
      };
    case actionTypes.GET_MARVELHERO_SUCCESS:
      return {
        ...state,
        heroes: {
          isFetching: false,
          data: [...state.heroes.data, action.payload],
        },
      };
    case actionTypes.GET_MARVELDETAILS_START:
      return {
        ...state,
        heroes: {
          ...state.heroes,
          isFetching: true,
        },
      };
    case actionTypes.GET_MARVELDETAILS_COMICS_SUCCESS:
      let newStateComics = [...state.heroes.data];

      let updatedStateComics = newStateComics.map((item) =>
        item.id === action.id ? { ...item, comicsDetail: action.payload } : item
      );

      return {
        ...state,
        heroes: {
          ...state.heroes,
          data: updatedStateComics,
        },
      };
    case actionTypes.GET_MARVELDETAILS_EVENTS_SUCCESS:
      let newStateEvents = [...state.heroes.data];

      let updatedStateEvents = newStateEvents.map((item) =>
        item.id === action.id ? { ...item, eventsDetail: action.payload } : item
      );

      return {
        ...state,
        heroes: {
          ...state.heroes,
          data: updatedStateEvents,
        },
      };
    case actionTypes.GET_MARVELDETAILS_SERIES_SUCCESS:
      let newStateSeries = [...state.heroes.data];

      let updatedStateSeries = newStateSeries.map((item) =>
        item.id === action.id ? { ...item, seriesDetail: action.payload } : item
      );

      return {
        ...state,
        heroes: {
          ...state.heroes,
          data: updatedStateSeries,
        },
      };

    case actionTypes.GET_MARVELDETAILS_SUCCESS:
      return {
        ...state,
        heroes: {
          ...state.heroes,
          isFetching: false,
        },
      };
    case actionTypes.CLEAR_MARVELHERO:
      return {
        ...state,
        heroes: {
          isFetching: true,
          data: [],
        },
      };
    default:
      return state;
  }
};

export default marvelReducer;
