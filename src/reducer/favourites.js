import actionTypes from "../actions/actionTypes";

const initialState = {
  books: [],
};

const favouritesReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_TO_FAVOURITES:
      return {
        ...state,
        books: [...state.books, action.payload],
      };
    case actionTypes.REMOVE_FROM_FAVOURITES:
      const itemIndex = state.books.findIndex(
        (item) => item.id === action.payload
      );
      let newState = [...state.books];
      newState.splice(itemIndex, 1);
      return {
        ...state,
        books: [...newState],
      };
    default:
      return state;
  }
};

export default favouritesReducer;
