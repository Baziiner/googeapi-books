import actionTypes from "../actions/actionTypes";

const initialState = {
  random: ["spider-man", "harry-potter", "war and peace"],
};

const randomReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_TO_RANDOM_LIST:
      if (action.payload.length < 2) {
        return {
          ...state,
        };
      } else {
        let valueAlreadyExists = state.random.indexOf(action.payload) > -1;
        let random = state.random.slice();

        if (valueAlreadyExists) {
          random = state.random.slice();
        } else {
          random.push(action.payload);
        }

        return {
          ...state,
          random,
        };
      }
    default:
      return state;
  }
};

export default randomReducer;
