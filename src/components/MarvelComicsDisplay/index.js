import React from "react";
import "./MarvelComicsDisplay.scss";
import "./MarvelComicsDisplayDark.scss";

const MarvelComicsDisplay = ({ data }) => {
  return (
    <div className="MarvelComicsDisplay">
      {data &&
        data.map((data, index) => (
          <div className="heroAvailable" key={index}>
            <div
              className="heroDetail-image"
              style={{
                backgroundImage: `url(${data.thumbnail.path}.${data.thumbnail.extension})`,
              }}
            ></div>
            <p>{data.title}</p>
          </div>
        ))}
    </div>
  );
};

export default MarvelComicsDisplay;
