import React from "react";
import "./CheckBox.scss";

const CheckBox = ({ handleChange, value }) => {
  return (
    <div className="CheckBox">
      <input
        type="checkbox"
        id="switch"
        onChange={(e) => handleChange(e.target.checked)}
        checked={value}
      />
      <label htmlFor="switch">Toggle</label>
    </div>
  );
};

export default CheckBox;
