import React from "react";
import { useSelector } from "react-redux";
import "./Loader.scss";

const Loader = ({ className }) => {
  const themeDark = useSelector((state) => state.theme.isDark);
  return (
    <div className={`Loader ${className || ""}`}>
      <svg viewBox="0 0 100 100">
        <defs>
          <filter id="shadow">
            <feDropShadow
              dx="0"
              dy="0"
              stdDeviation="1.5"
              floodColor={themeDark ? "#222222" : "#f4f4f7"}
            />
          </filter>
        </defs>
        <circle
          id="spinner"
          style={{
            fill: "transparent",
            stroke: `${themeDark ? "#121212" : "#2d92ff"}`,
            strokeWidth: "4px",
            strokeLinecap: "round",
            filter: "url(#shadow)",
          }}
          cx="50"
          cy="50"
          r="45"
        />
      </svg>
    </div>
  );
};

export default Loader;
