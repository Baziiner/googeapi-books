import React from "react";
import { NavLink } from "react-router-dom";
import "./NavButton.scss";
import "./NavButtonDark.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const NavButton = ({ to, title, icon, exact, className }) => (
  <NavLink
    to={to}
    className={`NavButton ${className || ""}`}
    activeClassName="is-active"
    exact={exact || false}
  >
    <button className="nav-button">
      {icon && <FontAwesomeIcon icon={icon} />}
      <span className="nav-link-title">{title}</span>
    </button>
  </NavLink>
);

export default NavButton;
