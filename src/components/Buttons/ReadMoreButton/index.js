import React from "react";
import "./ReadMoreButton.scss";
import "./ReadMoreButtonDark.scss";
import { Link } from "react-router-dom";

const ReadMoreButton = ({ to, className }) => {
  return (
    <button className={`ReadMoreButton ${className || ""}`}>
      <Link to={to}>Read More</Link>
    </button>
  );
};

export default ReadMoreButton;
