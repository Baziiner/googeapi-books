import React from "react";
import Loader from "../Loader";
import FeaturedItemBlock from "./FeaturedItemBlock";
import "./FeaturedItems.scss";

const FeaturedItems = ({ data }) => {
  return (
    <>
      {!data.isFetching ? (
        <div className="FeaturedItems">
          {data.items.map((data, index) => (
            <FeaturedItemBlock data={data} key={index} />
          ))}
        </div>
      ) : (
        <Loader />
      )}
    </>
  );
};

export default FeaturedItems;
