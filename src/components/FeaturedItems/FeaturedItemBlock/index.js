import React from "react";
import FeaturedBook from "../FeaturedBook";
import "./FeaturedItemBlock.scss";
import "./FeaturedItemBlockDark.scss";

const FeaturedItemBlock = ({ data }) => {
  return (
    <>
      {data.map((item, i) => (
        <div className="FeaturedItemBlock" key={i}>
          <div className="featured-item-wrapper">
            <div className="featured-items-title">
              <h2 className="featuted-title">{item.title}</h2>
            </div>
            <div className="featured-books">
              <FeaturedBook data={item.data} />
            </div>
          </div>
        </div>
      ))}
    </>
  );
};

export default FeaturedItemBlock;
