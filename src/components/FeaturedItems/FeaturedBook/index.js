import React from "react";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ReadMoreButton } from "../../Buttons";
import ROUTES from "../../../view/routes";
import NoImage from "../../../assets/images/empty.jpg";
import { useLocation } from "react-router-dom";
import "./FeaturedBook.scss";
import "./FeaturedBookDark.scss";

const FeaturedBook = ({ data }) => {
  const location = useLocation();
  return (
    <>
      {data.map((item, index) => (
        <div className="FeaturedBook" key={index}>
          <div
            className="FeaturedBookImage"
            style={{
              backgroundImage: `url(${
                item.volumeInfo.imageLinks
                  ? item.volumeInfo.imageLinks.smallThumbnail ||
                    item.volumeInfo.imageLinks.thumbnail
                  : NoImage
              })`,
            }}
          />
          <div className="featured-book-content">
            <h3 className="featured-book-title">
              {item.volumeInfo.subtitle || item.volumeInfo.title}
            </h3>
            <p className="featured-book-rating">
              <FontAwesomeIcon icon={faStar} className="fa-star" />
              {item.volumeInfo.averageRating}
            </p>
            <ReadMoreButton
              to={{
                pathname: `${ROUTES.DETAILS}`,
                search: `id=${item.id}`,
                backRoute: location.pathname,
              }}
              className="featured-read-more"
            />
          </div>
        </div>
      ))}
    </>
  );
};

export default FeaturedBook;
