import React from "react";
import { faHome, faHeart, faCog } from "@fortawesome/free-solid-svg-icons";
import { NavButton } from "../Buttons";
import ROUTES from "../../view/routes";
import "./Navigation.scss";
import "./NavigationDark.scss";

const Navigation = () => {
  return (
    <div className="Navigation">
      <div className="nav-category">
        <h4 className="nav-title">Discover</h4>
        <NavButton to={ROUTES.HOME} title="HOME" icon={faHome} exact />
        <NavButton to={ROUTES.MARVEL} title="MARVEL" className="-marvel" />
      </div>
      <div className="nav-category">
        <h4 className="nav-title">Library</h4>
        <NavButton to={ROUTES.FAVOURITES} title="FAVOURITES" icon={faHeart} />
      </div>
      <NavButton
        to={ROUTES.SETTINGS}
        title="SETTINGS"
        icon={faCog}
        className="settings"
      />
    </div>
  );
};

export default Navigation;
