import React from "react";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link, useLocation } from "react-router-dom";
import ROUTES from "../../view/routes";
import "./CustomSearch.scss";
import "./CustomSearchDark.scss";

const CustomSearch = ({ handleChange, data, displaySuggestion }) => {
  const location = useLocation();
  return (
    <div className="CustomSearch">
      <span className="search-icon">
        <FontAwesomeIcon icon={faSearch} />
      </span>
      <input
        type="search"
        className="SearchField"
        onChange={(e) => handleChange(e.target.value)}
        placeholder="Search Any Book..."
      />
      {displaySuggestion && (
        <div className="suggestions">
          {!data.isFetching && (
            <>
              {data.items.map((el, index) => (
                <Link
                  to={{
                    pathname: `${ROUTES.DETAILS}`,
                    search: `id=${el.id}`,
                    backRoute: location.pathname,
                  }}
                  key={index}
                  className="redirect-to-details"
                >
                  {el.volumeInfo.subtitle
                    ? `${el.volumeInfo.title}: ${el.volumeInfo.subtitle} `
                    : el.volumeInfo.title}
                </Link>
              ))}
            </>
          )}
        </div>
      )}
    </div>
  );
};

export default CustomSearch;
