import React from "react";
import Navigation from "../Navigation";
import "./Sidebar.scss";
import "./SidebarDark.scss";

const Sidebar = () => {
  return (
    <aside className="Sidebar">
      <h1 className="main-title">GBookz</h1>
      <Navigation />
    </aside>
  );
};

export default Sidebar;
