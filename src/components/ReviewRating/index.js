import React from "react";
import "./ReviewRating.scss";
import { faStar, faStarHalf } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import _ from "lodash";

const ReviewRating = ({ rating }) => {
  let card = [];
  _.times(Math.ceil(rating), (index) => {
    card.push(
      Math.ceil(rating) !== rating ? (
        Math.floor(rating) === index ? (
          <FontAwesomeIcon icon={faStarHalf} key={index} className="fa-star" />
        ) : (
          <FontAwesomeIcon icon={faStar} key={index} className="fa-star" />
        )
      ) : (
        <FontAwesomeIcon icon={faStar} key={index} className="fa-star" />
      )
    );
  });

  return <div className="ReviewRating">{card}</div>;
};
export default ReviewRating;
