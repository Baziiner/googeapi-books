import React from "react";
import ROUTES from "../../../view/routes";
import { ReadMoreButton } from "../../Buttons";
import { useLocation } from "react-router-dom";
import NoImage from "../../../assets/images/empty.jpg";
import "./BookViewGrid.scss";
import "./BookViewGridDark.scss";

const BookViewGrid = ({ data }) => {
  const bookData = data.volumeInfo;
  const location = useLocation();
  const shortDescription =
    bookData.description && bookData.description.slice(0, 100).concat("...");
  return (
    <div className="BookViewGrid">
      <div className="BookViewGrid-wrapper">
        <div className="BookView-top">
          <div
            className="BookViewGrid-left"
            style={{
              backgroundImage: `url(${
                bookData.imageLinks
                  ? bookData.imageLinks.smallThumbnail ||
                    bookData.imageLinks.thumbnail
                  : NoImage
              })`,
            }}
          ></div>
          <div className="BookItem-right">
            <h3 className="book-title">{bookData.title}</h3>
            {bookData.subtitle && (
              <h4 className="book-subtitle">{bookData.subtitle}</h4>
            )}
            <h5 className="book-author">
              By{" "}
              {bookData.authors
                ? bookData.authors.map((el, i) =>
                    i === bookData.authors.length - 1 ? (
                      <span key={i}> {el} </span>
                    ) : (
                      <span key={i}>{el}, </span>
                    )
                  )
                : bookData.publisher}
            </h5>
            <ReadMoreButton
              to={{
                pathname: `${ROUTES.DETAILS}`,
                search: `id=${data.id}`,
                backRoute: location.pathname,
              }}
            />
          </div>
        </div>
        <div className="BookItem-description">
          <h4 className="description-title">Description</h4>
          <p className="description-content">
            {shortDescription || "Missing Description"}
          </p>
        </div>
      </div>
    </div>
  );
};

export default BookViewGrid;
