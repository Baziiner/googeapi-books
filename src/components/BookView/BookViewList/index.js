import React from "react";
import ReviewRating from "../../ReviewRating";
import "./BookViewList.scss";
import "./BookViewListDark.scss";
import ROUTES from "../../../view/routes";
import { ReadMoreButton } from "../../Buttons";
import { useLocation } from "react-router-dom";
import NoImage from "../../../assets/images/empty.jpg";

const BookViewList = ({ data }) => {
  const bookData = data.volumeInfo;
  const location = useLocation();
  const shortDescription =
    bookData.description && bookData.description.slice(0, 100).concat("...");
  return (
    <div className="BookViewList">
      <div className="BookViewList-left">
        <div
          className="BookView-Image"
          style={{
            backgroundImage: `url(${
              bookData.imageLinks
                ? bookData.imageLinks.smallThumbnail ||
                  bookData.imageLinks.thumbnail
                : NoImage
            })`,
          }}
        />
        <div className="BookView-rating">
          <ReviewRating rating={bookData.averageRating} />
        </div>
      </div>
      <div className="BookViewList-right">
        <h3 className="book-title">{bookData.title}</h3>
        <h4 className="book-subtitle">{bookData.subtitle}</h4>
        <h4 className="book-author">
          {bookData.authors
            ? bookData.authors.map((el, i) =>
                i === bookData.authors.length - 1 ? (
                  <span key={i}> {el} </span>
                ) : (
                  <span key={i}>{el}, </span>
                )
              )
            : bookData.publisher}
        </h4>
        <h4 className="description-title">Description</h4>
        <p className="description-content">{shortDescription}</p>
        <ReadMoreButton
          to={{
            pathname: `${ROUTES.DETAILS}`,
            search: `id=${data.id}`,
            backRoute: location.pathname,
          }}
        />
      </div>
    </div>
  );
};

export default BookViewList;
