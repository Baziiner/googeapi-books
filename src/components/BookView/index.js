import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronLeft,
  faChevronRight,
  faBook,
} from "@fortawesome/free-solid-svg-icons";
import BookViewGrid from "./BookViewGrid";
import Loader from "../Loader";
import "./BookView.scss";
import "./BookViewDark.scss";

const getWidth = () =>
  window.innerWidth ||
  document.documentElement.clientWidth ||
  document.body.clientWidth;

const BookView = ({ data, className }) => {
  const [transition, setTransition] = useState({
    maxSteps: 0,
    activeStyle: 0,
  });

  let [width, setWidth] = useState(getWidth());

  useEffect(() => {
    const resizeListener = () => {
      setWidth(getWidth());
    };
    window.addEventListener("resize", resizeListener);
    if (width > 560) {
      setTransition({
        maxSteps: Math.floor(data.items.length / 3),
        activeStyle: 0,
      });
    } else {
      setTransition({
        maxSteps: data.items.length - 1,
        activeStyle: 0,
      });
    }
    return () => {
      window.removeEventListener("resize", resizeListener);
    };
  }, [data.items, data, width]);

  const handleClickForward = (move) => {
    if (transition.activeStyle !== transition.maxSteps * 100) {
      setTransition({
        ...transition,
        activeStyle: move,
      });
    }
  };

  const handleClickBackwards = (move) => {
    if (transition.activeStyle !== 0) {
      setTransition({
        ...transition,
        activeStyle: move,
      });
    }
  };

  return (
    <div className={`BookView ${className || ""}`}>
      <div className="button-wrapper">
        <div
          className={`BookView-button -left ${
            transition.activeStyle === 0 ? "is-disabled" : ""
          }`}
          onClick={() => handleClickBackwards(transition.activeStyle - 100)}
        >
          <FontAwesomeIcon icon={faChevronLeft} />
        </div>
        <div
          className={`BookView-button -right ${
            transition.activeStyle === transition.maxSteps * 100
              ? "is-disabled"
              : ""
          }`}
          onClick={() => handleClickForward(transition.activeStyle + 100)}
        >
          <FontAwesomeIcon icon={faChevronRight} />
        </div>
      </div>
      <div className="BookView-items">
        <div
          className="grid-view"
          style={{ left: `${-transition.activeStyle}%` }}
        >
          {data.error.isError ? (
            <div className="grid-error">
              <FontAwesomeIcon icon={faBook} />
              {data.error.errorMsg}
            </div>
          ) : data.isFetching ? (
            <Loader className="-custom-relative" />
          ) : (
            data.items.map((data, index) => (
              <BookViewGrid
                data={data}
                key={index}
                activeStyle={transition.activeStyle}
              />
            ))
          )}
        </div>
      </div>
    </div>
  );
};

export default BookView;
