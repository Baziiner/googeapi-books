export default {
  HOME: "/",
  DETAILS: "/details",
  FAVOURITES: "/favourites",
  SETTINGS: "/ettings",
  MARVEL: "/marvel",
};
