import React from "react";
import CheckBox from "../../components/CheckBox";
import { useDispatch, useSelector } from "react-redux";
import { setTheme } from "../../actions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import "./Settings.scss";
import "./SettingsDark.scss";

const Settings = () => {
  const dispatch = useDispatch();

  const handleChange = (data) => {
    dispatch(setTheme(data));
  };

  const theme = useSelector((state) => state.theme.isDark);

  return (
    <div className="Settings">
      <div className="Settings-content">
        <div className="toggle-wrapper">
          <p className="toggle-title">
            <FontAwesomeIcon icon={faEye} className="toggle-icon" />
            Theme
          </p>
          <div className="toggle-content">
            <span className="option -left">Light</span>
            <CheckBox handleChange={handleChange} value={theme} />
            <span className="option -right">Dark</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Settings;
