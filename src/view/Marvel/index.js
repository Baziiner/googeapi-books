import React, { useEffect, useCallback, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMarvelHero, getMarvelDetails } from "../../actions";
import MarvelComicsDisplay from "../../components/MarvelComicsDisplay";
import Loader from "../../components/Loader";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import "./Marvel.scss";
import "./MarvelDark.scss";

const Marvel = () => {
  const dispatch = useDispatch();
  const getHero = useCallback(
    (value) => {
      for (let i = 0; i < value.length; i++) {
        dispatch(getMarvelHero(value[i]));
      }
    },
    [dispatch]
  );

  const getDetails = useCallback(
    (id) => {
      dispatch(getMarvelDetails(id));
    },
    [dispatch]
  );

  const marvelHeroes = useSelector((state) => state.marvel.heroes);

  useEffect(() => {
    marvelHeroes.data.length === 0 &&
      getHero([
        "spider-man",
        "iron man",
        "hulk",
        "captain america",
        "wolverine",
        "storm",
      ]);
  }, [getHero, marvelHeroes.data.length]);

  const [showFull, setVisibility] = useState({
    isVisible: false,
    visible: -1,
  });

  const handleDetailsDisplay = (id, index) => {
    if (!showFull.isVisible) {
      getDetails(id);
      setVisibility({
        isVisible: true,
        visible: index,
      });
    }
  };

  const handleClose = () => {
    setVisibility({
      isVisible: false,
    });
  };

  return (
    <div className="Marvel">
      <div className="Marvel-content">
        {!marvelHeroes.isFetching ? (
          <>
            <h3 className="hero-title">CHOOSE A MARVEL CHARACTER</h3>
            <div className="hero-wrapper">
              {marvelHeroes.data.map((data, index) => (
                <div
                  className={`HeroContainer ${
                    showFull.isVisible && showFull.visible === index
                      ? "is-active"
                      : ""
                  }`}
                  key={index}
                >
                  <div
                    className="Hero"
                    onClick={() => handleDetailsDisplay(data.id, index)}
                  >
                    <div
                      className="hero-image"
                      style={{
                        backgroundImage: `url(${data.thumbnail.path}.${data.thumbnail.extension})`,
                      }}
                    />
                    <div className="hero-name">
                      <p>{data.name}</p>
                    </div>
                    {showFull.isVisible && (
                      <button
                        onClick={() => handleClose()}
                        className="hero-close"
                      >
                        <FontAwesomeIcon icon={faTimesCircle} />
                      </button>
                    )}
                  </div>
                  <div className="hero-extra-content">
                    <h3 className="hero-desc-title">Description</h3>
                    <p className="hero-desc">{data.description}</p>
                    <h4 className="hero-desc-title">Comics</h4>
                    <MarvelComicsDisplay data={data.comicsDetail} />
                    <h4 className="hero-desc-title">Events</h4>
                    <MarvelComicsDisplay data={data.eventsDetail} />
                    <h4 className="hero-desc-title">Series</h4>
                    <MarvelComicsDisplay data={data.seriesDetail} />
                  </div>
                </div>
              ))}
            </div>
          </>
        ) : (
          <Loader />
        )}
      </div>
    </div>
  );
};

export default Marvel;
