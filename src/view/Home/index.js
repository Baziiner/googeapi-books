import React, { useState, useEffect, useCallback } from "react";
import { Helmet } from "react-helmet-async";
import { getBooks, getFeaturedBookList, searchFromInput } from "../../actions";
import { useDispatch, useSelector } from "react-redux";
import BookView from "../../components/BookView";
import FeaturedItem from "../../components/FeaturedItems";
import CustomSearch from "../../components/CustomSearch";
import _ from "lodash";
import "./Home.scss";
import "./HomeDark.scss";

const Home = () => {
  const dispatch = useDispatch();
  const [displaySuggestion, setSuggestion] = useState(false);
  const booksList = useSelector((state) => state.book.data);
  const featuredList = useSelector((state) => state.book.featured);
  const randomList = useSelector((state) => state.randomPool.random);
  const randomItem = randomList[Math.floor(Math.random() * randomList.length)];

  const findParent = (e, className) => {
    let element = e;

    const reRunCode = () => {
      setSuggestion(false);
      element = element.parentElement;
      findParent(element, className);
    };

    e.tagName !== "BODY" &&
      (element.className === className ? setSuggestion(true) : reRunCode());
  };

  const getBookList = useCallback(
    (value) => {
      dispatch(getBooks(value));
    },
    [dispatch]
  );

  const getFeaturedList = useCallback(
    (value) => {
      for (let i = 0; i < value.length; i++) {
        dispatch(getFeaturedBookList(value[i]));
      }
    },
    [dispatch]
  );

  const searchOnChange = useCallback(
    (value) => {
      dispatch(searchFromInput(value));
    },
    [dispatch]
  );

  useEffect(() => {
    booksList.title !== randomItem && getBookList(randomItem);
    featuredList.items.length === 0 &&
      getFeaturedList(["war and peace", "spider-man"]);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getBookList, getFeaturedList]);

  const handleChange = useCallback(
    _.debounce((value) => searchOnChange(value || randomItem), 1000),
    []
  );

  return (
    <>
      <Helmet>
        <title> Home - GoogleAPI BOOKS</title>
      </Helmet>
      <content
        className="Home"
        onClick={(e) => findParent(e.target, "CustomSearch")}
      >
        <div className="header-home">
          <CustomSearch
            handleChange={handleChange}
            data={booksList}
            displaySuggestion={displaySuggestion}
          />
        </div>
        <div className="Home-content">
          <BookView data={booksList} className="-custom" />
          <h3 className="featured-home-title">Featured</h3>
          <div className="home-featured">
            <FeaturedItem data={featuredList} />
          </div>
        </div>
      </content>
    </>
  );
};

export default Home;
