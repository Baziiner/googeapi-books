import React from "react";
import { Helmet } from "react-helmet-async";
import "./Favourites.scss";
import "./FavouritesDark.scss";
import { useSelector } from "react-redux";
import BookViewList from "../../components/BookView/BookViewList";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSlash } from "@fortawesome/free-solid-svg-icons";

const Favourites = () => {
  const getFavourites = useSelector((state) => state.favourites.books);

  return (
    <>
      <Helmet>
        <title> Favourites - GoogleAPI BOOKS </title>
      </Helmet>
      <content className="Favourites">
        <div className="Favourites-content">
          {getFavourites.length ? (
            <>
              {getFavourites.map((data, index) => (
                <BookViewList data={data} key={index} />
              ))}
            </>
          ) : (
            <div className="empty-title">
              <span className="icon-wrapper">
                <FontAwesomeIcon icon={faSlash} />
              </span>
              <span>
                Seems like you have no Favourite Books. Make sure to add the
                books you like to favourites
              </span>
            </div>
          )}
        </div>
      </content>
    </>
  );
};

export default Favourites;
