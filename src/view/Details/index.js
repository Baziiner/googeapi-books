import React, { useEffect, useCallback } from "react";
import { useLocation, Redirect, Link } from "react-router-dom";
import { Helmet } from "react-helmet-async";
import { useDispatch, useSelector } from "react-redux";
import { getBookByID, addFavourite, removeFavourite } from "../../actions";
import ReviewRating from "../../components/ReviewRating";
import {
  faHeart,
  faPen,
  faShoppingCart,
  faChevronLeft,
  faSearch,
} from "@fortawesome/free-solid-svg-icons";
import Loader from "../../components/Loader";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ROUTES from "../routes";
import NoImage from "../../assets/images/empty.jpg";
import "./Details.scss";
import "./DetailsDark.scss";

const Details = ({ location }) => {
  const dispatch = useDispatch();

  const goBack = location.backRoute ? location.backRoute : ROUTES.HOME;

  function useQuery() {
    return new URLSearchParams(useLocation().search);
  }
  let query = useQuery();
  const activeQuery = query.get("id");
  const availableBook = useSelector((state) => state.book.bookID);
  const favouriteList = useSelector((state) => state.favourites.books);

  const getDetailedBook = useCallback(
    (value) => {
      dispatch(getBookByID(value));
    },
    [dispatch]
  );

  const handleRemove = (data) => {
    dispatch(removeFavourite(data));
  };

  useEffect(() => {
    getDetailedBook(activeQuery);
  }, [getDetailedBook, activeQuery]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  // useEffect(() => {
  // }, [favouriteList]);

  const handleAdd = (data) => {
    dispatch(addFavourite(data));
  };

  return (
    <>
      <Helmet>
        <title> Details - GoogleAPI BOOKS</title>
      </Helmet>
      {query.get("id") === undefined ? (
        <Redirect to="/" />
      ) : (
        <content className="Details">
          {availableBook.isFetching ? (
            <Loader className="-loader" />
          ) : (
            <>
              <div className="details-top">
                <div className="details-options">
                  <button className="button-back">
                    <Link to={goBack}>
                      <FontAwesomeIcon icon={faChevronLeft} />
                      <span>Back</span>
                    </Link>
                  </button>
                  <h2 className="details-page-title">
                    {availableBook.item.volumeInfo.title}
                  </h2>
                  <div className="details-page-search">
                    <FontAwesomeIcon icon={faSearch} />
                  </div>
                </div>
                <div
                  className="details-image"
                  style={{
                    backgroundImage: `url(${
                      availableBook.item.volumeInfo.imageLinks
                        ? availableBook.item.volumeInfo.imageLinks.thumbnail
                        : NoImage
                    })`,
                  }}
                />
                <div className="details-content">
                  <div className="details-content-top">
                    <div
                      className="details-content-image"
                      style={{
                        backgroundImage: `url(${
                          availableBook.item.volumeInfo.imageLinks
                            ? availableBook.item.volumeInfo.imageLinks.thumbnail
                            : NoImage
                        })`,
                      }}
                    >
                      {favouriteList &&
                        favouriteList.some(
                          (item) => item.id === availableBook.item.id
                        ) && (
                          <span className="favourited">
                            <FontAwesomeIcon icon={faHeart} />
                          </span>
                        )}
                    </div>
                    <div className="details-text">
                      <h3 className="details-title">
                        {availableBook.item.volumeInfo.title}
                      </h3>
                      {availableBook.item.volumeInfo.subtitle && (
                        <h4 className="details-subtitle">
                          {availableBook.item.volumeInfo.subtitle}
                        </h4>
                      )}
                      <h5 className="details-author">
                        By{" "}
                        {availableBook.item.volumeInfo.authors
                          ? availableBook.item.volumeInfo.authors.map((el, i) =>
                              i ===
                              availableBook.item.volumeInfo.authors.length -
                                1 ? (
                                <span key={i}> {el} </span>
                              ) : (
                                <span key={i}>{el}, </span>
                              )
                            )
                          : availableBook.item.volumeInfo.publisher}
                      </h5>
                      <ReviewRating
                        rating={availableBook.item.volumeInfo.averageRating}
                      />
                      <div className="details-purchase">
                        <button className="purchase-button">
                          <span className="purchase-icon">
                            <FontAwesomeIcon icon={faShoppingCart} />
                          </span>
                          GET NOW
                        </button>
                        {favouriteList &&
                        favouriteList.some(
                          (item) => item.id === availableBook.item.id
                        ) ? (
                          <button
                            className="details-favourite is-favourite"
                            onClick={() => handleRemove(availableBook.item.id)}
                          >
                            <FontAwesomeIcon icon={faHeart} />
                          </button>
                        ) : (
                          <button
                            className="details-favourite"
                            onClick={() => handleAdd(availableBook.item)}
                          >
                            <FontAwesomeIcon icon={faHeart} />
                          </button>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="details-content-bottom">
                    <span>
                      <FontAwesomeIcon icon={faPen} className="fa-pen" />
                      {availableBook.item.volumeInfo.ratingsCount
                        ? `${availableBook.item.volumeInfo.ratingsCount} `
                        : "No "}
                      Review
                      {availableBook.item.volumeInfo.ratingsCount === 1
                        ? ""
                        : "s"}
                    </span>
                    <p className="publish-date">
                      Published:{" "}
                      {availableBook.item.volumeInfo.publishedDate
                        .split("-")
                        .join("/")}
                    </p>
                  </div>
                </div>
              </div>
              <div className="details-synopsis">
                <h3 className="synopsis-title">Description</h3>
                <p className="synopsis-content">
                  {availableBook.item.volumeInfo.description}
                </p>
              </div>
            </>
          )}
        </content>
      )}
    </>
  );
};

export default Details;
